﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace Cranes
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = new StreamReader("input.txt");
            int S = int.Parse(reader.ReadLine());
            reader.Close();

            int Katya, Petya, Sergey;
            Katya = (S/3)*2;
            Petya = Katya/2/2;
            Sergey = Petya;

            StreamWriter writer = new StreamWriter("output.txt");
            writer.WriteLine($"{Petya} --- {Katya} --- {Sergey}");
            writer.Close();
            
        }
    }
}
